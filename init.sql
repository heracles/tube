create table log(
LOG_ID bigint not null AUTO_INCREMENT,
event varchar(100),
event_time datetime,
PRIMARY KEY (`LOG_ID`)
);

create table tube_log(
tube_log_id bigint not null AUTO_INCREMENT,
log_id bigint not null,
direction int,
PRIMARY KEY (`tube_LOG_ID`),
  KEY `tube_log_FK` (`log_id`),
  CONSTRAINT `tube_log_FK` FOREIGN KEY (`tube_LOG_ID`) REFERENCES `log` (`LOG_ID`)
);

create table passenger_log(
pass_log_id bigint not null AUTO_INCREMENT,
log_id bigint not null,
in_out varchar(3),
count int,
PRIMARY KEY (`pass_LOG_ID`),
  KEY `pass_log_FK` (`log_id`),
  CONSTRAINT `pass_log_FK` FOREIGN KEY (`pass_LOG_ID`) REFERENCES `log` (`LOG_ID`)

);