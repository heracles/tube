drop table if exists passenger_log;
drop table if exists tube_log;
drop table if exists log;

create table log(
LOG_ID bigint not null AUTO_INCREMENT,
event varchar(100),
event_time datetime,
PRIMARY KEY (`LOG_ID`)
);

create table tube_log(
tube_log_id bigint not null AUTO_INCREMENT,
log_id bigint not null,
direction int,
PRIMARY KEY (`tube_LOG_ID`),
FOREIGN KEY (`LOG_ID`) REFERENCES `log` (`LOG_ID`)
);

create table passenger_log(
pass_log_id bigint not null AUTO_INCREMENT,
log_id bigint not null,
in_out varchar(3),
count int,
PRIMARY KEY (`pass_LOG_ID`),
 FOREIGN KEY (`LOG_ID`) REFERENCES `log` (`LOG_ID`)
);