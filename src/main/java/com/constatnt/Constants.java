package com.constatnt;

public final class Constants {
    public static final int FIRST_FLOOR = 0;
    public static final int HEIGHT_FLOOR = 10;
    public static final int COUNT_PASSENGERS = 4;
    public static final int UP_DIRECTION = 0;
    public static final int DOWN_DIRECTION = 1;

    public static final double WIDTH = 70;
    public static final double HEIGHT = 30;
    public static final double MONITOR_WIDTH = 110;
    public static final double MONITOR_HEIGHT = 40;

    public static final double INFO_In_TUBE_WIDTH = 40;
    public static final double INFO_WAITE_WIDTH = 50;

    public static final int START_LAYOUT_X = 40;
    public static final int START_LAYOUT_Y = 10;
    public static final int LAYOUT_X = 50;
    public static final int LAYOUT_Y = 10;
    public static final int LAYOUT_INFO_X = 185;
    public static final int LAYOUT_INFO_Y = 13;
    public static final int LAYOUT_INFO_HEADER_Y = 30;

    public static final int HUMAN_WAY = 30;

    public static final String UP = "⮙";
    public static final String DOWN = "⮛";
}
