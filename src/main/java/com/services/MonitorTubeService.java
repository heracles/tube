package com.services;

import java.util.concurrent.atomic.AtomicIntegerArray;

public interface MonitorTubeService {
    int getFloor();
    int getCountWaitPassengers(int direction);
    int getCountInTubePassengers();
    AtomicIntegerArray getInTubePassengers();

    void changeWaitThreads(int direction, boolean increasing);
    void incInTubeThreads(int direction);
    void decInTubeThreads();
    int getInTubeThreads();
    int getWaitThreads(int direction);

    int getNextOnDirectionFloor(int direction);
    int getHighestLowestFloor(int direction);
    void setFloor(int floor);
    void setDirection(int direction);
}
