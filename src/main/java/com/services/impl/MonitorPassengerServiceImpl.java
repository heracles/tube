package com.services.impl;

import com.model.Monitor;
import com.services.MonitorPassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

@Service
public class MonitorPassengerServiceImpl implements MonitorPassengerService {
    private final Monitor monitor;

    @Autowired
    public MonitorPassengerServiceImpl(Monitor monitor){
        this.monitor = monitor;
    }

    @Override
    public int getFloor() {
        return monitor.getFloor();
    }

    @Override
    public int getDirection() {
        return monitor.getDirection();
    }

    @Override
    public AtomicIntegerArray getWaitPassengers(int direction){
        return monitor.getWaitPassengers(direction);
    }

    @Override
    public void changeWaitThreads(int direction, boolean increasing){
        monitor.changeWaitThreads(direction, increasing);
    };

    @Override
    public int getWaitThreads(int direction){
        return monitor.getWaitThreads(direction);
    }

    @Override
    public ReentrantLock getLock() {
        return monitor.getLock();
    }

    @Override
    public Condition getLockTube() {
        return monitor.getLockTube();
    }

    @Override
    public Condition getLockPassenger() {
        return monitor.getLockPassenger();
    }

    @Override
    public void printQueue() {
        monitor.printQueue();
    }
}
