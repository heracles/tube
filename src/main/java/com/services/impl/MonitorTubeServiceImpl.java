package com.services.impl;

import com.model.Monitor;
import com.services.MonitorTubeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicIntegerArray;

@Service
public class MonitorTubeServiceImpl implements MonitorTubeService {
    private Monitor monitor;

    @Override
    public int getFloor() {
        return monitor.getFloor();
    }

    @Override
    public void setFloor(int floor){
        monitor.setFloor(floor);
    }

    @Override
    public void setDirection(int direction){
        monitor.setDirection(direction);
    }

    @Override
    public int getCountWaitPassengers(int direction) {
        return monitor.getCountPassengers(direction);
    }

    @Override
    public int getCountInTubePassengers(){
        return monitor.getCountInTubePassengers();
    }

    @Override
    public AtomicIntegerArray getInTubePassengers(){
        return monitor.getInTubePassengers();
    }

    @Override
    public void changeWaitThreads(int direction, boolean increasing){
        monitor.changeWaitThreads(direction, increasing);
    };

    @Override
    public void incInTubeThreads(int direction){
        monitor.incInTubeThreads();
    };

    @Override
    public void decInTubeThreads(){
        monitor.decInTubeThreads();
    };

    @Override
    public int getInTubeThreads(){
        return monitor.getInTubeThreads();
    }

    @Override
    public int getWaitThreads(int direction){
        return monitor.getWaitThreads(direction);
    }

    @Override
    public int getNextOnDirectionFloor(int direction) {
        return monitor.getNextOnDirectionFloor(direction);
    }

    @Override
    public int getHighestLowestFloor(int direction) {
        return monitor.getHighestLowestFloor(direction);
    }

    //*******************************************************************************

    @Autowired
    public void setMonitor(Monitor monitor){
        this.monitor = monitor;
    }
}
