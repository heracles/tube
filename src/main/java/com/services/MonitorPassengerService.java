package com.services;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public interface MonitorPassengerService {
    int getFloor();
    int getDirection();
    AtomicIntegerArray getWaitPassengers(int direction);

    void changeWaitThreads(int direction, boolean increasing);
    int getWaitThreads(int direction);

    ReentrantLock getLock();
    Condition getLockTube();
    Condition getLockPassenger();

    void printQueue();
}
