package com.dao.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import java.util.Properties;

@ConfigurationProperties
public class DBConfiguration {
    private final DSConfig datasource;
    private C3p0Config c3p0;
    private final JpaConfig jpa;

    public DBConfiguration (DSConfig datasource, C3p0Config c3p0, JpaConfig jpaConfig){
        this.datasource = datasource;
        this.c3p0 = c3p0;
        this.jpa = jpaConfig;
    }

    public DSConfig getDatasource() {
        return datasource;
    }

    public JpaConfig getJpa() {
        return jpa;
    }

    public C3p0Config getC3p0() {
        return c3p0;
    }

    @ConfigurationProperties(prefix="spring.datasource")
    public static class DSConfig {
        private String driverClassName;
        private String url;
        private String userName;
        private String password;

        public String getDriverClassName() {
            return driverClassName;
        }

        public void setDriverClassName(String driverClassName) {
            this.driverClassName = driverClassName;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    @ConfigurationProperties(prefix="spring.jpa.properties.hibernate.c3p0")
    public static class C3p0Config{
        private int maxPoolSize;
        private int minPoolSize;
        private int initialPoolSize;
        private int acquireIncrement;
        private int idleConnectionTestPeriod;
        private int maxStatements;
        private int maxIdleTime;

        public int getMaxPoolSize() {
            return maxPoolSize;
        }

        public void setMaxPoolSize(int maxPoolSize) {
            this.maxPoolSize = maxPoolSize;
        }

        public int getMinPoolSize() {
            return minPoolSize;
        }

        public void setMinPoolSize(int minPoolSize) {
            this.minPoolSize = minPoolSize;
        }

        public int getInitialPoolSize() {
            return initialPoolSize;
        }

        public void setInitialPoolSize(int initialPoolSize) {
            this.initialPoolSize = initialPoolSize;
        }

        public int getAcquireIncrement() {
            return acquireIncrement;
        }

        public void setAcquireIncrement(int acquireIncrement) {
            this.acquireIncrement = acquireIncrement;
        }

        public int getIdleConnectionTestPeriod() {
            return idleConnectionTestPeriod;
        }

        public void setIdleConnectionTestPeriod(int idleConnectionTestPeriod) {
            this.idleConnectionTestPeriod = idleConnectionTestPeriod;
        }

        public int getMaxStatements() {
            return maxStatements;
        }

        public void setMaxStatements(int maxStatements) {
            this.maxStatements = maxStatements;
        }

        public int getMaxIdleTime() {
            return maxIdleTime;
        }

        public void setMaxIdleTime(int maxIdleTime) {
            this.maxIdleTime = maxIdleTime;
        }
    }

    @ConfigurationProperties(prefix="spring")
    public static class JpaConfig{
        private Properties jpa;

        public Properties getJpa() {
            return jpa;
        }

        public void setJpa(Properties jpa) {
            this.jpa = jpa;
        }
    }
}
