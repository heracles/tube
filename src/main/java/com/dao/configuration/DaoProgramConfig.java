package com.dao.configuration;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableConfigurationProperties({DBConfiguration.class, DBConfiguration.DSConfig.class,
        DBConfiguration.JpaConfig.class, DBConfiguration.C3p0Config.class})
//@ConfigurationPropertiesScan({"com.dao.configuration"})
//@Profile("dev")
@PropertySource("application-${spring.profiles.active}.properties")
public class DaoProgramConfig {

    private DBConfiguration dbConfiguration;

    @Autowired
    Environment env;


    public DaoProgramConfig(DBConfiguration dbConfiguration){
        this.dbConfiguration = dbConfiguration;
    }

    /*@Bean
    @Lazy
    public SessionFactory sessionFactory(){
        Configuration config = new Configuration();
        config.addAnnotatedClass(Log.class);
        return config.buildSessionFactory(new StandardServiceRegistryBuilder().build());
    }*/

    /*@Bean
    @Autowired
    public LocalSessionFactoryBean SessionFactory(DataSource dataSource) {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setPackagesToScan("com.dao.entities");
        sessionFactory.setHibernateProperties(dbConfiguration.getJpa().getProperties());

        return sessionFactory;
    }*/

    @Bean
    public JpaVendorAdapter jpaVendorAdapter(){
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setDatabasePlatform(dbConfiguration.getJpa().getJpa().getProperty("dialect"));
        jpaVendorAdapter.setGenerateDdl(false);
        jpaVendorAdapter.setShowSql(Boolean.parseBoolean(dbConfiguration.getJpa().getJpa().getProperty("show_sql")));
        return jpaVendorAdapter;
    }

    @Bean
    public DataSource dataSource() {
        ComboPooledDataSource ds = DataSourceBuilder.create()
                .driverClassName(dbConfiguration.getDatasource().getDriverClassName())
                .url(dbConfiguration.getDatasource().getUrl())
                .username(dbConfiguration.getDatasource().getUserName())
                .password(dbConfiguration.getDatasource().getPassword())
                .type(ComboPooledDataSource.class)
                .build();

        ds.setMaxPoolSize(dbConfiguration.getC3p0().getMaxPoolSize());
        ds.setMinPoolSize(dbConfiguration.getC3p0().getMinPoolSize());
        ds.setInitialPoolSize(dbConfiguration.getC3p0().getInitialPoolSize());
        ds.setAcquireIncrement(dbConfiguration.getC3p0().getAcquireIncrement());
        ds.setIdleConnectionTestPeriod(dbConfiguration.getC3p0().getIdleConnectionTestPeriod());
        ds.setMaxStatements(dbConfiguration.getC3p0().getMaxStatements());
        ds.setMaxIdleTime(dbConfiguration.getC3p0().getMaxIdleTime());
        return ds;
    }

    @Bean
    @Autowired
    @Primary
    public EntityManagerFactory entityManagerFactory(DataSource dataSource, JpaVendorAdapter jpaVendorAdapter) {
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSource);
        emf.setJpaVendorAdapter(jpaVendorAdapter);
        emf.setPackagesToScan("com.dao.entities");
        //emf.setPersistenceUnitName("default");
        Properties jpsProperties = new Properties();
        jpsProperties.setProperty("hibernate.hbm2ddl.auto", "none");
        emf.setJpaProperties(jpsProperties);
        emf.afterPropertiesSet();
        return emf.getObject();
    }

    @Bean
    public SessionFactory setSessionFactory(EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.unwrap(SessionFactory.class);
    }

}
