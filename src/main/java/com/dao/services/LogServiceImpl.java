package com.dao.services;

import com.dao.entities.Log;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LogServiceImpl implements LogService{
    private SessionFactory sessionFactory;

    @Autowired
    public LogServiceImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save (Log log){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(log);
        tx.commit();
        session.close();
    }
}
