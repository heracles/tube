package com.handlers;

public interface TriConsumer {
    void accept(int direction, int floor, int count);
}
