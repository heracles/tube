package com.handlers;

import com.model.Monitor;
import com.visualization.services.TubeVisualizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.function.BiConsumer;


@Service
public class HandlerOnGoOut implements BiConsumer<Integer, Integer> {

    private TubeVisualizationService tubeVisualization;
    private Monitor monitor;

    @Override
    public void accept(Integer floor, Integer amount) {
        monitor.getInTubePassengers().getAndAdd(floor, -amount);

        tubeVisualization.startHumanMoving(false, amount);
        tubeVisualization.setInTubePassengerAmount(monitor.getCountInTubePassengers());
        tubeVisualization.decInTubeThreads();
    }

    //*******************************************************************************

    @Autowired
    public void setTubeVisualization(TubeVisualizationService tubeVisualization) {
        this.tubeVisualization = tubeVisualization;
    }

    @Autowired
    public void setMonitor(Monitor monitor) {
        this.monitor = monitor;
    }
}
