package com.handlers;

import com.services.MonitorTubeService;
import com.visualization.services.TubeVisualizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("HandlerOnComeIn")
public class HandlerOnComeIn implements TriConsumer {

    private TubeVisualizationService tubeVisualization;
    private MonitorTubeService monitor;
    private TriConsumer handlerOnWait;

    @Override
    public void accept(int direction, int nextFloor, int amount) {
        //monitor.changeWaitThreads(direction, false);
        monitor.getInTubePassengers().getAndAdd(nextFloor, amount);

        tubeVisualization.startHumanMoving(true, amount);
        tubeVisualization.setInTubePassengerAmount(monitor.getCountInTubePassengers());
        tubeVisualization.incInTubeThreads();

        handlerOnWait.accept(direction, monitor.getFloor(), -amount);
    }

    //*******************************************************************************

    @Autowired
    public void setTubeVisualization(TubeVisualizationService tubeVisualization) {
        this.tubeVisualization = tubeVisualization;
    }

    @Autowired
    public void setMonitor(MonitorTubeService monitor) {
        this.monitor = monitor;
    }

    @Autowired
    @Qualifier("HandlerOnWait")
    public void setHandlerOnWait(TriConsumer handlerOnWait) {
        this.handlerOnWait = handlerOnWait;
    }
}
