package com.handlers;

import java.util.function.Consumer;

public interface OnChangeDirection extends Consumer<Integer> {
    @Override
    void accept(Integer direction);
}
