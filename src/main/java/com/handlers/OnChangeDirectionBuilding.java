package com.handlers;

import com.visualization.model.BuildingFx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.constatnt.Constants.*;

@Service
public class OnChangeDirectionBuilding implements OnChangeDirection {

    private BuildingFx building;

    @Override
    public void accept(Integer direction) {
        building.setCurrentDirection(direction);
        if (direction == UP_DIRECTION)
            building.getDisplay().getDirectionText().setText("       " + UP);
        else
            building.getDisplay().getDirectionText().setText("       " + DOWN);
    }

    //*******************************************************************************

    @Autowired
    public void setBuilding(BuildingFx building) {
        this.building = building;
    }
}