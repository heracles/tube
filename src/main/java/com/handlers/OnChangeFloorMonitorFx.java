package com.handlers;

import com.visualization.model.DisplayFx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OnChangeFloorMonitorFx implements OnFloorArrived{

    private DisplayFx monitor;

    @Override
    public void accept(Integer floor) {
        monitor.getCurrentFloor().setText(String.valueOf(floor + 1));
    }

    //*******************************************************************************

    @Autowired
    public void setMonitor(DisplayFx monitor) {
        this.monitor = monitor;
    }
}