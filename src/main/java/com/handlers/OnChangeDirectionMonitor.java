package com.handlers;

import com.services.MonitorTubeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OnChangeDirectionMonitor implements OnChangeDirection{

    private MonitorTubeService monitor;

    @Override
    public void accept(Integer direction) {
        monitor.setDirection(direction);
    }

    //*******************************************************************************

    @Autowired
    public void setMonitor(MonitorTubeService monitor) {
        this.monitor = monitor;
    }
}
