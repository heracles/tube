package com.handlers;

import com.services.MonitorTubeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OnChangeFloorMonitor implements OnChangeFloor {
    private MonitorTubeService monitor;

    @Override
    public void accept(Integer floor) {
        monitor.setFloor(floor);
    }

    //*******************************************************************************

    @Autowired
    public void setMonitor(MonitorTubeService monitor) {
        this.monitor = monitor;
    }
}