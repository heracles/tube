package com.handlers;

import java.util.function.Consumer;

public interface OnChangeFloor extends Consumer<Integer> {
    void accept(Integer floor);
}
