package com.handlers;

import com.services.MonitorPassengerService;
import com.visualization.services.TubeVisualizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("HandlerOnWait")
public class HandlerOnWait implements TriConsumer {

    private TubeVisualizationService tubeVisualization;
    private MonitorPassengerService monitor;

    @Override
    public void accept(int direction, int floor, int count) {
        monitor.changeWaitThreads(direction, count > 0);
        monitor.getWaitPassengers(direction).getAndAdd(floor, count);

        tubeVisualization.setWaitPassengerAmount(direction, floor, monitor.getWaitPassengers(direction).get(floor));
        tubeVisualization.changeWaitThreads(direction, monitor.getWaitThreads(direction));
    }

    //*******************************************************************************

    @Autowired
    public void setTubeVisualization(TubeVisualizationService tubeVisualization) {
        this.tubeVisualization = tubeVisualization;
    }

    @Autowired
    public void setMonitor(MonitorPassengerService monitor) {
        this.monitor = monitor;
    }
}
