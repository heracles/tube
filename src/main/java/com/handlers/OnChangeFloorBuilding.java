package com.handlers;

import com.visualization.model.BuildingFx;
import com.visualization.services.TubeVisualizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OnChangeFloorBuilding implements OnChangeFloor {
    private TubeVisualizationService tubeVisualization;
    private BuildingFx building;

    @Override
    public void accept(Integer floor) {
        building.setCurrentFloor(floor);
        tubeVisualization.startTubeMoving();
    }

    //*******************************************************************************

    @Autowired
    public void setTubeVisualization(TubeVisualizationService tubeVisualization) {
        this.tubeVisualization = tubeVisualization;
    }

    @Autowired
    public void setBuilding(BuildingFx building) {
        this.building = building;
    }
}
