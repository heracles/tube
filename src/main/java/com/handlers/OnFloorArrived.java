package com.handlers;

import java.util.function.Consumer;

public interface OnFloorArrived extends Consumer<Integer> {
    void accept(Integer floor);
}
