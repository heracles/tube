package com.visualization.services.impl;

import com.visualization.BuildingHelper;
import com.visualization.model.BuildingFx;
import com.visualization.services.TubeVisualizationService;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.concurrent.Semaphore;
import static com.constatnt.Constants.*;
import static java.lang.String.format;

@Service
public class TubeVisualization implements TubeVisualizationService {
    private BuildingFx building;
    private Semaphore syncPassenger;

    // *******************************************************************************

    @Autowired
    public void setBuilding(BuildingFx building) {
        this.building = building;
    }

    @Autowired
    public void setSyncPassenger(Semaphore syncPassenger) {
        this.syncPassenger = syncPassenger;
    }

    //=================================================================================

    @Override
    public void setInTubePassengerAmount(Integer amount) {
        ((Text) building.getTube().getRight()).setText(String.valueOf(amount));
    }

    @Override
    public void startHumanMoving(boolean comeIn, int amount) {
        try {
            syncPassenger.acquire();
            building.getHuman().setComeIn(comeIn);
            building.getHuman().getHumanMoving().setByX(comeIn ? -HUMAN_WAY : HUMAN_WAY);
            BuildingHelper.setHumanLocation(building.getHuman(), building.getFloors(), building.getCurrentFloor());
            if (comeIn) {
                //building.getHuman().setText("<=🚶=");
                building.getHuman().setText(format("<=%d=", amount));
            } else {
                //building.getHuman().setText("=🚶=>");
                building.getHuman().setText(format("=%d=>", amount));
            }
            building.getHuman().setVisible(true);
            building.getHuman().getHumanMoving().playFromStart();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public int getRealFloor() {
        return building.getCurrentFloor() + 1;
    }

    @Override
    public void startTubeMoving() {
        building.getTube().getTubeMoving().setByY((building.getCurrentDirection() == UP_DIRECTION ? -1 : 1) * HEIGHT);
        building.getDisplay().getVisualDirection().playFromStart();
        building.getTube().getTubeMoving().playFromStart();
    }

    @Override
    public void setWaitPassengerAmount(int direction, int floor, int count){
        TextFlow textMonitor = (TextFlow) building.getPassengerInfo().getChildren().get(floor);
        if (direction == UP_DIRECTION) {
            BuildingHelper.setUpCount(count, (Text)textMonitor.getChildren().get(0));
        } else
            BuildingHelper.setDownCount(count, (Text)textMonitor.getChildren().get(2));
    }

    @Override
    public void changeWaitThreads(int direction, int amount){
        Text wait = (Text) ((VBox)((StackPane) building.getInfoDisplay().getChildren().get(1))
                .getChildren().get(1)).getChildren().get(direction);
        wait.setText(String.valueOf(amount) + " " + (direction == UP_DIRECTION ? UP : DOWN));
    }

    @Override
    public void incInTubeThreads(){
        changeInTubeThreads(true);
    }

    @Override
    public void decInTubeThreads(){
        changeInTubeThreads(false);
    }

    //=================================================================================

    private void changeInTubeThreads(boolean increasing){
        byte inc = increasing ? (byte)1 : (byte)-1;
        Text wait = (Text) ((StackPane) building.getInfoDisplay().getChildren().get(0)).getChildren().get(1);
        wait.setText(String.valueOf(Integer.parseInt(wait.getText()) + inc));
    }
}
