package com.visualization.services;

public interface TubeVisualizationService {
    void startHumanMoving(boolean comeIn, int amount);
    void setInTubePassengerAmount(Integer amount);
    void setWaitPassengerAmount(int direction, int floor, int count);
    void startTubeMoving();
    int getRealFloor();

    void changeWaitThreads(int direction, int amount);
    void incInTubeThreads();
    void decInTubeThreads();
}
