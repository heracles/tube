package com.visualization;

import com.handlers.OnFloorArrived;
import com.visualization.model.BuildingFx;
import com.visualization.services.impl.TubeVisualization;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import static com.constatnt.Constants.*;

@Component
public class TubeBuildingFx {

    private BuildingFx building;
    private TubeVisualization tubeVisualization;
    private CyclicBarrier syncTube;
    private OnFloorArrived handlerOnFloorArrived;

    private Button btnContinue = new Button ( "Continue" ) ;
    private Button btnPause = new Button ( "Pause" ) ;
    private Button btnExit = new Button ( "Exit" ) ;

    @Autowired
    public void setBuilding(BuildingFx building) {
        this.building = building;
    }

    @Autowired
    public void setTubeVisualization(TubeVisualization tubeVisualization) {
        this.tubeVisualization = tubeVisualization;
    }

    @Autowired
    public void setSyncTube(CyclicBarrier syncTube) {
        this.syncTube = syncTube;
    }

    @Autowired
    public void setHandlerOnFloorArrived(OnFloorArrived handlerOnFloorArrived) {
        this.handlerOnFloorArrived = handlerOnFloorArrived;
    }

    //*******************************************************************************

    public Scene build(){
        building.setRootNode(new Pane());
        building.setScene(new Scene(building.getRootNode(), 300, 440));
        building.getScene().getStylesheets().add("ApplicationFx.css");
        building.getRootNode().setId("root-node");

        finalizeBuilding();

        return building.getScene();
    }

    public void buildMovingTube(){
        building.getTube().setTubeMoving(new TranslateTransition(Duration.millis(1000), building.getTube()));
        building.getTube().getTubeMoving().setFromY(0);
        building.getTube().getTubeMoving().setByY((building.getCurrentDirection() == UP_DIRECTION ? -1 : 1) * HEIGHT);
        building.getTube().getTubeMoving().setCycleCount(1);
        building.getTube().getTubeMoving().setAutoReverse(false);

        building.getTube().getTubeMoving().setOnFinished(t1 -> {
            if (building.getTube().getTranslateY() == (building.getCurrentDirection() == UP_DIRECTION ? -1 : 1) * HEIGHT){
                handlerOnFloorArrived.accept(building.getCurrentFloor());
                building.getTube().getTubeMoving().stop();
                building.getDisplay().getVisualDirection().stop();
                BuildingHelper.setTubeLocation(building.getTube(), building.getFloors(), building.getCurrentFloor());

                try {
                    syncTube.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }

                ((Text) building.getTube().getCenter()).setText(String.valueOf(tubeVisualization.getRealFloor()));
            }
        });
    }

    private void buildButtonExit(){
        btnExit.setLayoutX(230);
        btnExit.setLayoutY(410);
        btnExit.setMinWidth(20);
        btnExit.setMinHeight(10);
        btnExit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
            }
        });

        building.getRootNode().getChildren().add(btnExit);
    }

    private void buildButtonContinue(){
        btnContinue.setLayoutX(30);
        btnContinue.setLayoutY(410);
        btnContinue.setMinWidth(20);
        btnContinue.setMinHeight(10);
        btnContinue.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                building.getDisplay().getVisualDirection().playFromStart();
            }
        });

        building.getRootNode().getChildren().add(btnContinue);
    }

    private void buildButtonPause(){
        btnPause.setLayoutX(130);
        btnPause.setLayoutY(410);
        btnPause.setMinWidth(20);
        btnPause.setMinHeight(10);
        btnPause.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
            }
        });

        building.getRootNode().getChildren().add(btnPause);
    }

    private void finalizeBuilding(){
        // for MONITOR
        // set Monitor text based on direction
        if (building.getCurrentDirection() == UP_DIRECTION)
            building.getDisplay().getDirectionText().setText("       " + UP);
        else
            building.getDisplay().getDirectionText().setText("       " + DOWN);
        building.getRootNode().getChildren().add(building.getDisplay());
        //-------------------------------------------------------------------------------
        building.getRootNode().getChildren().add(building.getInfoDisplay());
        //-------------------------------------------------------------------------------
        // for FLOORS
        building.getRootNode().getChildren().add(building.getFloors());
        //-------------------------------------------------------------------------------
        // for TUBE
        // set current floor's number
        ((Text) building.getTube().getCenter()).setText(String.valueOf(tubeVisualization.getRealFloor()));
        buildMovingTube();
        building.getFloors().getChildren().add(building.getTube());
        //-------------------------------------------------------------------------------
        // for Passengers info
        building.getRootNode().getChildren().addAll(building.getPassengerInfo().getLines());
        building.getRootNode().getChildren().add(building.getPassengerInfo());
        //-------------------------------------------------------------------------------
        // for HUMAN
        building.getFloors().getChildren().add(building.getHuman());
        //-------------------------------------------------------------------------------
        // for Buttons
        buildButtonPause();
        buildButtonContinue();
        buildButtonExit();
    }
}
