package com.visualization.model;

import com.visualization.BuildingHelper;
import javafx.animation.TranslateTransition;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.Semaphore;
import static com.constatnt.Constants.HUMAN_WAY;

@Component
public class HumanFx extends Text {

    private boolean isComeIn = true;
    private TranslateTransition humanMoving;
    private FloorsFx floors;
    private Semaphore syncPassenger;

    //-------------------------------------------------------------------------------

    @Autowired
    public void setFloors(FloorsFx floors) {
        this.floors = floors;
    }

    @Autowired
    public void setSyncPassenger(Semaphore syncPassenger) {
        this.syncPassenger = syncPassenger;
    }

    public void setComeIn(boolean comeIn) {
        isComeIn = comeIn;
    }

    public boolean isComeIn() {
        return isComeIn;
    }

    public TranslateTransition getHumanMoving() {
        return humanMoving;
    }

    //-------------------------------------------------------------------------------
    @PostConstruct
    private void build() {
        setId("pass-visual");
        BuildingHelper.setHumanLocation(this, floors, 0);
        setVisible(false);
        buildMovingPassenger();
    }

    private void buildMovingPassenger(){
        humanMoving = new TranslateTransition(Duration.millis(2000), this);
        humanMoving.setFromX(0);
        humanMoving.setCycleCount(1);
        humanMoving.setAutoReverse(false);

        humanMoving.setOnFinished(t1 -> {
            if (getTranslateX() == (isComeIn ? -HUMAN_WAY : HUMAN_WAY)){
                humanMoving.stop();
                setVisible(false);
                syncPassenger.release();
            }
        });
    }

}
