package com.visualization.model;

import com.visualization.BuildingHelper;
import javafx.animation.TranslateTransition;
import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import static com.constatnt.Constants.*;

@Component
public class TubeFx extends BorderPane{

    private TranslateTransition tubeMoving;
    private FloorsFx floors;

    //-------------------------------------------------------------------------------

    @Autowired
    public void setFloors(FloorsFx floors) {
        this.floors = floors;
    }

    public TranslateTransition getTubeMoving() {
        return tubeMoving;
    }

    public void setTubeMoving(TranslateTransition tubeMoving) {
        this.tubeMoving = tubeMoving;
    }

    //-------------------------------------------------------------------------------
    @PostConstruct
    private void buildTube(){
        setId("tube-node");
        setMinHeight(HEIGHT);
        setMinWidth(WIDTH);

        Text currentFloor = new Text();
        currentFloor.setId("text-tube-floor");
        setCenter(currentFloor);
        BorderPane.setAlignment(currentFloor, Pos.CENTER);

        currentFloor = new Text("0");
        currentFloor.setId("text-tube-passenger");
        setRight(currentFloor);

        BorderPane.setAlignment(currentFloor, Pos.TOP_RIGHT);
        BuildingHelper.setTubeLocation(this, floors, 0);

        //buildMovingTube(floors);
    }
}
