package com.visualization.model;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BuildingFx {
    private Scene scene;
    private Pane rootNode;
    private DisplayFx display;
    private InfoDisplayFx infoDisplay;
    private FloorsFx floors;
    private TubeFx tube;
    private PassengerInfoFx passengerInfo;
    private HumanFx human;

    private int currentFloor;
    private int currentDirection;

    //************************************************************************

    @Autowired
    public void setDisplay(DisplayFx display) {
        this.display = display;
    }

    @Autowired
    public void setFloors(FloorsFx floors) {
        this.floors = floors;
    }

    @Autowired
    public void setTube(TubeFx tube) {
        this.tube = tube;
    }

    @Autowired
    public void setPassengerInfo(PassengerInfoFx passengerInfo) {
        this.passengerInfo = passengerInfo;
    }

    @Autowired
    public void setHuman(HumanFx human) {
        this.human = human;
    }

    @Autowired
    public void setInfoDisplay(InfoDisplayFx infoDisplay) {
        this.infoDisplay = infoDisplay;
    }

    //************************************************************************

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public Pane getRootNode() {
        return rootNode;
    }

    public void setRootNode(Pane rootNode) {
        this.rootNode = rootNode;
    }

    public DisplayFx getDisplay() {
        return display;
    }

    public InfoDisplayFx getInfoDisplay() {
        return infoDisplay;
    }

    public FloorsFx getFloors() {
        return floors;
    }

    public TubeFx getTube() {
        return tube;
    }

    public PassengerInfoFx getPassengerInfo() {
        return passengerInfo;
    }

    public HumanFx getHuman() {
        return human;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public int getCurrentDirection() {
        return currentDirection;
    }

    public void setCurrentDirection(int currentDirection) {
        this.currentDirection = currentDirection;
    }

    //************************************************************************
}
