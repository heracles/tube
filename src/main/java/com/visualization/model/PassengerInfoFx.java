package com.visualization.model;

import com.visualization.BuildingHelper;
import javafx.scene.Group;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import static com.constatnt.Constants.*;
import static com.constatnt.Constants.HEIGHT;

@Component
public class PassengerInfoFx extends Group{

    private List<Line> lines = new ArrayList<>();

    public PassengerInfoFx(){
        build();
    }

    //-------------------------------------------------------------------------------

    public List<Line> getLines() {
        return lines;
    }

    public void setLines(List<Line> lines) {
        this.lines = lines;
    }

    //-------------------------------------------------------------------------------

    private void build(){
        setLayoutX(LAYOUT_INFO_X);
        setLayoutY(START_LAYOUT_Y + MONITOR_HEIGHT + LAYOUT_Y + LAYOUT_INFO_Y);

        Text passagerInfo;
        Line line;
        TextFlow textMonitor;
        for (int i = HEIGHT_FLOOR; i > FIRST_FLOOR; i--){
            textMonitor = new TextFlow();
            textMonitor.setLayoutY((i-1) * HEIGHT + LAYOUT_INFO_HEADER_Y);

            passagerInfo = new Text();
            BuildingHelper.setUpCount(0, passagerInfo);
            textMonitor.getChildren().add(passagerInfo);

            passagerInfo = new Text();
            passagerInfo.setText(" - ");
            passagerInfo.setId("pass-info-hyphen");
            textMonitor.getChildren().add(passagerInfo);

            passagerInfo = new Text();
            BuildingHelper.setDownCount(0, passagerInfo);
            textMonitor.getChildren().add(passagerInfo);

            getChildren().add(textMonitor);

            line = new Line();
            line.setStartX(START_LAYOUT_X + WIDTH);
            line.setStartY(START_LAYOUT_Y + MONITOR_HEIGHT + LAYOUT_Y + LAYOUT_INFO_HEADER_Y + i * HEIGHT);
            line.setEndX(LAYOUT_INFO_X + 60);
            line.setEndY(START_LAYOUT_Y + MONITOR_HEIGHT + LAYOUT_Y + LAYOUT_INFO_HEADER_Y + i * HEIGHT);
            line.getStrokeDashArray().addAll(2d, 10d);
            lines.add(line);
        }
        textMonitor = new TextFlow();
        textMonitor.setLayoutY(0);
        passagerInfo = new Text();
        passagerInfo.setText(UP + "      ");
        passagerInfo.setId("pass-info1");
        textMonitor.getChildren().add(passagerInfo);
        passagerInfo = new Text();
        passagerInfo.setText(DOWN);
        passagerInfo.setId("pass-info2");
        textMonitor.getChildren().add(passagerInfo);
        getChildren().add(textMonitor);
    }
}
