package com.visualization.model;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.springframework.stereotype.Component;

import static com.constatnt.Constants.*;
import static javafx.animation.Animation.INDEFINITE;

@Component
public class DisplayFx extends StackPane {
    private Text directionText;
    private Text currentFloor;
    private Timeline visualDirection;

    public DisplayFx(){
        build();
    }

    //-------------------------------------------------------------------------------

    public Text getDirectionText() {
        return directionText;
    }

    public void setDirectionText(Text directionText) {
        this.directionText = directionText;
    }

    public Timeline getVisualDirection() {
        return visualDirection;
    }

    public void setVisualDirection(Timeline visualDirection) {
        this.visualDirection = visualDirection;
    }

    public Text getCurrentFloor() {
        return currentFloor;
    }

    //-------------------------------------------------------------------------------

    private void build(){
        //"0 ↑⮙🠝🢁🚶"
        //"↓⮛🠟🢃"
        setLayoutX(60);
        setLayoutY(START_LAYOUT_Y);
        setMinHeight(MONITOR_HEIGHT);
        Rectangle background = new Rectangle(MONITOR_WIDTH, MONITOR_HEIGHT);
        background.setFill(new LinearGradient(0,0,0,1, true, CycleMethod.NO_CYCLE,
                new Stop(0, Color.web("#4977A3")),
                new Stop(0.5, Color.web("#B0C6DA")),
                new Stop(1,Color.web("#9CB6CF"))));
        background.setStroke(Color.web("#D0E6FA"));
        background.setArcHeight(3.5);
        background.setArcWidth(3.5);

        currentFloor = new Text("1");
        currentFloor.setId("text-monitor-floor");
        directionText = new Text("       " + UP);
        directionText.setId("text-monitor-direction");

        getChildren().addAll(background, currentFloor, directionText);

        buildShowingDirection();

    }

    private void buildShowingDirection(){
        visualDirection = new Timeline(
                new KeyFrame(Duration.millis(200), event -> {
                    if (!directionText.isUnderline()) {
                        directionText.toBack();
                        directionText.setUnderline(true);
                    }
                    else {
                        directionText.toFront();
                        directionText.setUnderline(false);
                    }
                }));
        visualDirection.setCycleCount(INDEFINITE);
    }
}
