package com.visualization.model;

import javafx.scene.Group;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import org.springframework.stereotype.Component;
import static com.constatnt.Constants.*;

@Component
public class FloorsFx extends Group{

    public FloorsFx(){
        build();
    }

    //-------------------------------------------------------------------------------

    private void build(){
        //---------------------------------------------------
       // groupFloors = new Group();
        setLayoutX(START_LAYOUT_X);
        setLayoutX(START_LAYOUT_X);;
        setLayoutY(START_LAYOUT_Y + MONITOR_HEIGHT + LAYOUT_Y + LAYOUT_INFO_HEADER_Y);
        Rectangle floor;
        for (int i = HEIGHT_FLOOR; i > FIRST_FLOOR; i--){
            buildFloor(i);
        }
    }

    private void buildFloor(int floorNumber){
        StackPane floor = new StackPane();
        Rectangle floorRec = new Rectangle();
        floorRec.setId("floor-node");
        floorRec.setWidth(WIDTH);
        floorRec.setHeight(HEIGHT);
        floor.setLayoutY((floorNumber-1) * HEIGHT);

        Text currentFloor = new Text(String.valueOf(HEIGHT_FLOOR - floorNumber + 1));
        currentFloor.setId("pass-info-floor");
        floor.getChildren().addAll(floorRec, currentFloor);
        getChildren().add(floor);
    }
}
