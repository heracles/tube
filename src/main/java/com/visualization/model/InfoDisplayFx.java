package com.visualization.model;

import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import org.springframework.stereotype.Component;
import static com.constatnt.Constants.*;
import static com.constatnt.Constants.MONITOR_HEIGHT;

@Component
public class InfoDisplayFx extends HBox {

    public InfoDisplayFx(){
        build();
    }

    //-------------------------------------------------------------------------------

    private void build(){

        setLayoutX(200);
        setLayoutY(START_LAYOUT_Y);

        StackPane wait = new StackPane();
        wait.setAlignment(Pos.CENTER);

        Text passenger = new Text();
        passenger.setText("0");
        passenger.setId("text-tube-floor");

        Rectangle background = new Rectangle(INFO_In_TUBE_WIDTH, MONITOR_HEIGHT);
        background.setId("tube-node");
        background.setArcHeight(3.5);
        background.setArcWidth(3.5);

        wait.getChildren().addAll(background, passenger);
        getChildren().add(wait);
        //--------------------------------------------------
        StackPane inTube = new StackPane();
        inTube.setAlignment(Pos.CENTER_RIGHT);

        VBox info = new VBox();
        info.setAlignment(Pos.CENTER);
        passenger = new Text();
        passenger.setText("0 " + DOWN);
        passenger.setId("pass-info-floor");
        info.getChildren().add(passenger);
        passenger = new Text();
        passenger.setText("0 " + UP);
        passenger.setId("pass-info-floor");
        info.getChildren().add(passenger);

        background = new Rectangle(INFO_WAITE_WIDTH, MONITOR_HEIGHT);
        background.setId("floor-node");
        background.setArcHeight(3.5);
        background.setArcWidth(3.5);

        inTube.getChildren().addAll(background, info);
        getChildren().add(inTube);
    }
}
