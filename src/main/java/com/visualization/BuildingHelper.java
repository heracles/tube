package com.visualization;

import com.visualization.model.FloorsFx;
import com.visualization.model.HumanFx;
import com.visualization.model.TubeFx;
import javafx.scene.Node;
import javafx.scene.text.Text;
import static com.constatnt.Constants.*;
import static java.lang.String.format;

public class BuildingHelper {

    //-------------------------------------------------------------------------------

    public static void setTubeLocation(TubeFx tube, FloorsFx floors, int currentFloor) {
        tube.setTranslateX(0);
        tube.setTranslateY(0);
        Node floor = floors.getChildren().get(currentFloor);
        tube.setLayoutX(floor.getLayoutX());
        tube.setLayoutY(floor.getLayoutY());
    }

    public static void setHumanLocation(HumanFx human, FloorsFx floors, int currentFloor) {
        human.setTranslateX(0);
        human.setTranslateY(0);
        Node floor = floors.getChildren().get(currentFloor);
        human.setLayoutX(floor.getLayoutX() + (human.isComeIn() ? LAYOUT_INFO_X - START_LAYOUT_X - 40 : WIDTH));
        human.setLayoutY(floor.getLayoutY() + (human.isComeIn() ? 25 : 15));
    }

    public static void setDownCount(int downCount, Text passagerInfo) {
        passagerInfo.setText(format("%3s", downCount == 0 ? "" : downCount));
        passagerInfo.setId("pass-info2");

    }

    public static void setUpCount(int upCount, Text passagerInfo) {
        passagerInfo.setText(format("%-3s", upCount == 0 ? "" : upCount));
        passagerInfo.setId("pass-info1");
    }
}
