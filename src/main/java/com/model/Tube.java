package com.model;

import com.dao.entities.Log;
import com.dao.services.LogService;
import com.handlers.OnChangeDirection;
import com.handlers.OnChangeFloor;
import com.services.MonitorPassengerService;
import com.services.MonitorTubeService;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import static com.constatnt.Constants.*;
import static java.lang.String.format;

public class Tube extends Thread {
    private CyclicBarrier syncTube;
    private List<OnChangeFloor> handlersOnChangeFloor;
    private List<OnChangeDirection> handlersOnChangeDirection;


    private LogService logService;

    private int floor = FIRST_FLOOR;
    private int direction = UP_DIRECTION;
    private int directFloor = -1;
    private MonitorTubeService monitorTube;
    private MonitorPassengerService monitorPassenger;

    //=================================================================================

    public void setLogService(LogService logService) {
        this.logService = logService;
    }

    public void setSyncTube(CyclicBarrier syncTube) {
        this.syncTube = syncTube;
    }

    public void setMonitorTube(MonitorTubeService monitorTube) {
        this.monitorTube = monitorTube;
    }

    public void setMonitorPassenger(MonitorPassengerService monitorPassenger) {
        this.monitorPassenger = monitorPassenger;
    }

    public void setHandlersOnChangeFloor(List<OnChangeFloor> handlersOnChangeFloor) {
        this.handlersOnChangeFloor = handlersOnChangeFloor;
    }

    public void setHandlersOnChangeDirection(List<OnChangeDirection> handlersOnChangeDirection) {
        this.handlersOnChangeDirection = handlersOnChangeDirection;
    }

    //*****************************************************************

    public void run(){
        int oldDirection;

        monitorPassenger.printQueue();
        do {
            if (isInterrupted()) {
                break;
            }
            try {
                //*********************************************************
                monitorPassenger.getLock().lock();
                try {
                    while (monitorTube.getCountWaitPassengers(direction) != 0) {
                        Thread.sleep(500);
                        System.out.println(format("\rTube wait passengers on %s", floor));
                        monitorPassenger.getLockPassenger().signalAll();
                        monitorPassenger.getLockTube().await(2, TimeUnit.SECONDS);
                        monitorPassenger.printQueue();
                    }
                    oldDirection = direction;
                    if (checkCalls(true) && oldDirection != direction) {
                        //tube changes direction
                        while (monitorTube.getCountWaitPassengers(direction) != 0) {
                            Thread.sleep(500);
                            System.out.println(format("\rTube wait passengers on %s", floor));
                            monitorPassenger.getLockPassenger().signalAll();
                            monitorPassenger.getLockTube().await(2, TimeUnit.SECONDS);
                            monitorPassenger.printQueue();
                        }
                    }
                } finally {
                    monitorPassenger.getLock().unlock();
                }

                //**********************************************
                //
                monitorPassenger.getLock().lock();
                try {
                    if (!checkCalls(true)) {
                        System.out.println(format("\rTube stopped and waited passengers on %s floor", floor));
                        Thread.sleep(500);
                        continue;
                    }
                } finally {
                    monitorPassenger.getLock().unlock();
                }
                //**********************************************
                Thread.sleep(500);
                setNextFloor();
                try {
                    syncTube.await();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
                System.out.print(format("\rTube on %s", floor));

                //--------------------------------------------
                //to wait passengers
                monitorPassenger.getLock().lock();
                try {
                    if (getNextOnDirectionFloor() == floor) {
                        //Checking if tube has to stop on floor
                        System.out.println(format("\rTube stopped on %s", floor));
                        monitorPassenger.getLockPassenger().signalAll();
                    }
                } finally {
                    monitorPassenger.getLock().unlock();
                }
            } catch (InterruptedException e) {
                interrupt();
            }
        } while (true);
        //===========================================
    }

    //*********************************************************************

    private boolean checkCalls(boolean checkOnDirection){
        boolean isCalls = true;
        int currDirection = direction;
        if (directFloor == floor)
            directFloor = -1;
        int nextFloor = getNextOnDirectionFloor();
        if (nextFloor == -1){
            //Check passengers on higher/lower floor waiting opposed way
            nextFloor = monitorTube.getHighestLowestFloor(direction);
            if (nextFloor != -1){
                directFloor = nextFloor;
            } else if(checkOnDirection) {
                //Change way and Check opposed way
                switchDirection();
                isCalls = checkCalls(false);
            } else {
                isCalls = false;
                if (floor == FIRST_FLOOR)
                    direction = UP_DIRECTION;
                else if (floor == HEIGHT_FLOOR - 1)
                    direction = DOWN_DIRECTION;
            }
        }
        if (currDirection != direction) {
            handlersOnChangeDirection.forEach(handler -> handler.accept(direction));
        }
        return isCalls;
    }

    private int getNextOnDirectionFloor(){
        int nextFloor = monitorTube.getNextOnDirectionFloor(direction);
        if (nextFloor == -1 && directFloor != -1)
            nextFloor = directFloor;
        return nextFloor;
    }

    private void switchDirection(){
        if (direction == UP_DIRECTION){
            direction = DOWN_DIRECTION;
        } else {
            direction = UP_DIRECTION;
        }
    }

    private void setNextFloor(){
        if (direction == UP_DIRECTION)
            floor++;
        else floor--;
        Log log = new Log();
        log.setEvent("EEE");
        log.setEventTime(LocalDateTime.now());
        logService.save(log);
        handlersOnChangeFloor.forEach(handler -> handler.accept(floor));
    }
}
