package com.model;

import org.springframework.stereotype.Component;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;
import static com.constatnt.Constants.*;
import static java.lang.String.format;

@Component
public class Monitor {
    private volatile int floor = 0;
    private volatile int direction = 0;

    private volatile boolean finished = false;

    private final AtomicIntegerArray waitUpPassengers;
    private final AtomicIntegerArray waitDownPassengers;
    private final AtomicIntegerArray inTubePassengers;

    private final ReentrantLock lock;
    private final Condition lockPassenger;
    private final Condition lockTube;

    private AtomicInteger waitThreadsUp;
    private AtomicInteger waitThreadsDown;
    private AtomicInteger inTubeThreads;

    public Monitor(){
        lock = new ReentrantLock();
        lockPassenger = lock.newCondition();
        lockTube = lock.newCondition();
        waitUpPassengers = new AtomicIntegerArray(HEIGHT_FLOOR);
        initPassengers(waitUpPassengers);
        waitDownPassengers = new AtomicIntegerArray(HEIGHT_FLOOR);
        initPassengers(waitDownPassengers);
        inTubePassengers = new AtomicIntegerArray(HEIGHT_FLOOR);
        initPassengers(inTubePassengers);

        waitThreadsUp = new AtomicInteger(0);
        waitThreadsDown = new AtomicInteger(0);
        inTubeThreads = new AtomicInteger(0);
    }

    //=================================================================================

    private void initPassengers(AtomicIntegerArray passengers){
        for (int i = 0; i < HEIGHT_FLOOR; i++)
            passengers.set(i, 0);
    }

    //=================================================================================
    // For passenger's service

    public int getFloor() {
        return floor;
    }

    public int getDirection() {
        return direction;
    }

    public AtomicIntegerArray getWaitPassengers(int direction){
        if (direction == UP_DIRECTION)
            return waitUpPassengers;
        else return waitDownPassengers;
    }

    public ReentrantLock getLock() {
        return lock;
    }

    public Condition getLockTube() {
        return lockTube;
    }

    public Condition getLockPassenger() {
        return lockPassenger;
    }

    public AtomicIntegerArray getWaitPassengers(){
        return getWaitPassengers(direction);
    }

    public int getCountInTubePassengers() {
        return IntStream.range(0, 10).reduce(0, (left, right) -> left + inTubePassengers.get(right));
    }

    public void changeWaitThreads(int direction, boolean increasing){
        int inc = increasing ? 1 : -1;
        if (direction == UP_DIRECTION)
            waitThreadsUp.addAndGet(inc);
        else
            waitThreadsDown.addAndGet(inc);
    };

    public int getWaitThreads(int direction){
        return direction == UP_DIRECTION ? waitThreadsUp.get() : waitThreadsDown.get();
    }

    //========================================================================
    // For tube's service

    public int getCountWaitPassengers(){
        return getWaitPassengers().get(floor);
    }

    public int getCountPassengers(int direction){
        return getWaitPassengers(direction).get(floor) + inTubePassengers.get(floor);
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public AtomicIntegerArray getInTubePassengers() {
        return inTubePassengers;
    }

    public void incInTubeThreads(){
        inTubeThreads.incrementAndGet();
    };

    public void decInTubeThreads(){
        inTubeThreads.decrementAndGet();
    };

    public int getInTubeThreads() {
        return inTubeThreads.get();
    }

    //========================================================================

    public int getOnDirectionPassengers(int direction, int floor){
        return getWaitPassengers(direction).get(floor) + inTubePassengers.get(floor);
    }

    public int getNextOnDirectionFloor(int direction){
        int nextFloor = -1;
        if (direction == UP_DIRECTION){
            for (int i = floor; i < HEIGHT_FLOOR; i++){
                if (getOnDirectionPassengers(direction, i) != 0) {
                    nextFloor = i;
                    break;
                }
            }
        } else {
            for (int i = floor; i >= FIRST_FLOOR; i--) {
                if (getOnDirectionPassengers(direction, i) != 0) {
                    nextFloor = i;
                    break;
                }
            }
        }
        return nextFloor;
    }

    public int getHighestLowestFloor(int direction){
        int nextFloor = -1;
        if (direction == UP_DIRECTION){
            for (int i = HEIGHT_FLOOR - 1; i > floor; i--){
                if (getOnDirectionPassengers(DOWN_DIRECTION, i) != 0) {
                    nextFloor = i;
                    break;
                }
            }
        } else {
            for (int i = FIRST_FLOOR; i < floor; i++) {
                if (getOnDirectionPassengers(UP_DIRECTION, i) != 0) {
                    nextFloor = i;
                    break;
                }
            }
        }
        return nextFloor;
    }

    //========================================================================

    public void printQueue(){
        System.out.println("------------------------------------------------");
        System.out.print("-> : ");
        for (int i = 0; i < HEIGHT_FLOOR; i++) {
            System.out.print(format("%s-", waitUpPassengers.get(i)));
        }
        System.out.println();
        System.out.print("<- : ");
        for (int i = 0; i < HEIGHT_FLOOR; i++) {
            System.out.print(format("%s-", waitDownPassengers.get(i)));
        }
        System.out.println();
        System.out.println("------------------------------------------------");
    }
}
