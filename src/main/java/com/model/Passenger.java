package com.model;

import com.handlers.HandlerOnComeIn;
import com.services.MonitorPassengerService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.function.BiConsumer;
import static com.constatnt.Constants.DOWN_DIRECTION;
import static com.constatnt.Constants.UP_DIRECTION;
import static java.lang.String.format;

public class Passenger extends Thread {
    private final List<HandlerOnComeIn> handlerOnComeIn;
    private final List<BiConsumer<Integer, Integer>> handlerOnGoOut;

    private final int count;
    private final int currFloor;
    private final int direction;
    private final int floor;
    private final MonitorPassengerService monitorPassengerService;
    private final Semaphore syncPassenger;

    public Passenger (int count, int direction, int currFloor, int floor,
                      MonitorPassengerService monitorPassengerService,
                      Semaphore syncPassenger,
                      List<HandlerOnComeIn> handlerOnComeIn,
                      List<BiConsumer<Integer, Integer>> handlerOnGoOut){
        this.count = count;
        this.currFloor = currFloor;
        this.direction = direction;
        this.floor = floor;
        this.monitorPassengerService = monitorPassengerService;
        this.syncPassenger = syncPassenger;
        this.handlerOnComeIn = handlerOnComeIn;
        this.handlerOnGoOut = handlerOnGoOut;
    }

    private void checkInterrupted(){
        if (isInterrupted())
            interrupt();
    }

    @Override
    public void run(){
        monitorPassengerService.getLock().lock();
        try {
            //tube is on higher floor
            while (monitorPassengerService.getDirection() != direction ||
                    (direction == UP_DIRECTION && monitorPassengerService.getDirection() == direction && monitorPassengerService.getFloor() > currFloor) ||
                    (direction == DOWN_DIRECTION && monitorPassengerService.getDirection() == direction && monitorPassengerService.getFloor() < currFloor)){
                checkInterrupted();
                monitorPassengerService.getLockPassenger().await();
            }

            //tube is on lower floor
            while ((direction == UP_DIRECTION && monitorPassengerService.getDirection() == direction && monitorPassengerService.getFloor() < currFloor) ||
                    (direction == DOWN_DIRECTION && monitorPassengerService.getDirection() == direction && monitorPassengerService.getFloor() > currFloor)){
                checkInterrupted();
                monitorPassengerService.getLockPassenger().await();
            }

            //tube is on floor where passengers are waiting
            if (monitorPassengerService.getDirection() == direction && monitorPassengerService.getFloor() == currFloor){
                System.out.println(format("Passenger N%s set on floor %s /-> %s to %s/", Thread.currentThread().getName(), currFloor, count, floor));
                handlerOnComeIn.forEach(handler -> handler.accept(direction, floor, count));
                syncPassenger.acquire();
                syncPassenger.release();
                monitorPassengerService.getLockTube().signal();
            }

            //tube drives passengers
            while ((direction == UP_DIRECTION && monitorPassengerService.getDirection() == direction && monitorPassengerService.getFloor() < floor) ||
                    (direction == DOWN_DIRECTION && monitorPassengerService.getDirection() == direction && monitorPassengerService.getFloor() > floor)){
                checkInterrupted();
                monitorPassengerService.getLockPassenger().await();
            }

            //tube is on floor where passengers will goo out
            if (monitorPassengerService.getDirection() == direction && monitorPassengerService.getFloor() == floor){
                System.out.println(format("Passenger N%s go out on floor %s /%s/", Thread.currentThread().getName(), floor, count));
                handlerOnGoOut.forEach(handler -> handler.accept(floor, count));
                syncPassenger.acquire();
                syncPassenger.release();
                monitorPassengerService.getLockTube().signal();
            }

        } catch (InterruptedException e) {
            //e.printStackTrace();
        } finally {
            monitorPassengerService.getLock().unlock();
        }
    }
}
