package com.model;

import com.handlers.HandlerOnComeIn;
import com.services.MonitorPassengerService;
import com.handlers.TriConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.function.BiConsumer;

import static com.constatnt.Constants.*;
import static java.lang.String.format;

@Service
public class PassengerManager extends Thread{
    private TriConsumer handlerOnWait;
    private List<HandlerOnComeIn> handlerOnComeIn;
    private List<BiConsumer<Integer, Integer>> handlerOnGoOut;
    private Semaphore syncPassenger;

    protected MonitorPassengerService monitorPassenger;
    private final List<Passenger> passengers;

    public PassengerManager (MonitorPassengerService monitorPassenger){
        this.monitorPassenger = monitorPassenger;
        passengers = new LinkedList<>();
    }

    @Autowired
    @Qualifier("HandlerOnWait")
    public void setHandlerOnWait(TriConsumer handlerOnWait) {
        this.handlerOnWait = handlerOnWait;
    }

    @Autowired
    public void setHandlerOnComeIn(List<HandlerOnComeIn> handlerOnComeIn) {
        this.handlerOnComeIn = handlerOnComeIn;
    }

    @Autowired
    public void setHandlerOnGoOut(List<BiConsumer<Integer, Integer>> handlerOnGoOut) {
        this.handlerOnGoOut = handlerOnGoOut;
    }

    @Autowired
    @Qualifier("syncPassenger")
    public void setSyncPassenger(Semaphore syncPassenger) {
        this.syncPassenger = syncPassenger;
    }

    //=================================================================================

    private void generate() {
        Random random = new Random();
        int times = random.nextInt(5);
        times++;
        for (int i = 0; i < times; i++) {
            if (isInterrupted()){
                interruptPassengers();
                return;
            } else {
                int currFloor = random.nextInt(HEIGHT_FLOOR);
                currFloor = currFloor + 1 == HEIGHT_FLOOR ? currFloor - 1 : currFloor;
                int nextFloor = random.nextInt(HEIGHT_FLOOR);
                int count = random.nextInt(COUNT_PASSENGERS) + 1;
                nextFloor = currFloor == nextFloor ? nextFloor + 1 : nextFloor;

                int direction = getDirection(currFloor, nextFloor);
                System.out.println(format("Passenger N%s /%s/: on floor %s to %s /%s", i, count, currFloor, nextFloor, (direction == 0 ? "->" : "<-")));
                Passenger passenger = new Passenger(count, direction, currFloor, nextFloor, monitorPassenger,
                        syncPassenger, handlerOnComeIn, handlerOnGoOut);
                passenger.setName(String.valueOf(i));
                int finalCurrFloor = currFloor;
                handlerOnWait.accept(direction, finalCurrFloor, count);
                passengers.add(passenger);
                passenger.start();
            }
        }
    }

    private int getDirection(int currFloor, int nextFloor){
        if (currFloor < nextFloor)
            return UP_DIRECTION;
        else return DOWN_DIRECTION;
    }

    private void interruptPassengers() {
        for (Passenger passenger : passengers) {
            if (passenger.isAlive()) {
                passenger.interrupt();
            }
        }
    }

    private void removeInactivePassengers() {
        passengers.removeIf(passenger -> !passenger.isAlive());
    }

    public void run(){
        while (true) {
            try {
                System.out.println(format("###%s: Generate new passengers", LocalDateTime.now()));
                generate();
                Thread.sleep(20000);
                removeInactivePassengers();
            } catch (InterruptedException e) {
                interruptPassengers();
                return;
            }
        }
    }
}
