package com.model;

import com.dao.services.LogService;
import com.handlers.OnChangeDirection;
import com.handlers.OnChangeFloor;
import com.services.MonitorPassengerService;
import com.services.MonitorTubeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.CyclicBarrier;

@Component
public class TubeBuilder {

    private LogService logService;
    private CyclicBarrier syncTube;
    private MonitorTubeService monitorTube;
    private MonitorPassengerService monitorPassenger;
    private List<OnChangeFloor> handlersOnChangeFloor;
    private List<OnChangeDirection> handlersOnChangeDirection;

    private static Tube tube;

    @Autowired
    public void setLogService(LogService logService) {
        this.logService = logService;
    }
    @Autowired
    public void setSyncTube(CyclicBarrier syncTube) {
        this.syncTube = syncTube;
    }
    @Autowired
    public void setMonitorTube(MonitorTubeService monitorTube) {
        this.monitorTube = monitorTube;
    }
    @Autowired
    public void setMonitorPassenger(MonitorPassengerService monitorPassenger) {
        this.monitorPassenger = monitorPassenger;
    }
    @Autowired
    public void setHandlersOnChangeFloor(List<OnChangeFloor> handlersOnChangeFloor) {
        this.handlersOnChangeFloor = handlersOnChangeFloor;
    }
    @Autowired
    public void setHandlersOnChangeDirection(List<OnChangeDirection> handlersOnChangeDirection) {
        this.handlersOnChangeDirection = handlersOnChangeDirection;
    }

    @PostConstruct
    private Tube build(){
        tube = new Tube();
        tube.setLogService(logService);
        tube.setSyncTube(syncTube);
        tube.setMonitorTube(monitorTube);
        tube.setMonitorPassenger(monitorPassenger);
        tube.setHandlersOnChangeFloor(handlersOnChangeFloor);
        tube.setHandlersOnChangeDirection(handlersOnChangeDirection);

        return tube;
    }

    public Tube getTube() {
        return tube;
    }
}
