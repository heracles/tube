package com;

import com.model.TubeBuilder;
import com.model.PassengerManager;
import com.visualization.TubeBuildingFx;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

public class ApplicationFx extends Application {
    private ConfigurableApplicationContext appContext;

    private TubeBuilder tubeBuilder;
    private PassengerManager passengerManager;
    private TubeBuildingFx tubeBuildingFx;

    @Override
    public void init(){
        String[] args = getParameters().getRaw().toArray(new String[0]);
        appContext = new SpringApplicationBuilder().sources(App.class).run(args);

        tubeBuildingFx = appContext.getBean(TubeBuildingFx.class);
        passengerManager = appContext.getBean(PassengerManager.class);
        tubeBuilder = appContext.getBean(TubeBuilder.class);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Tube emulation");
        primaryStage.setResizable(false);
        primaryStage.setScene(tubeBuildingFx.build());
        passengerManager.start();
        tubeBuilder.getTube().start();

        primaryStage.show();
    }

    @Override
    public void stop(){
        tubeBuilder.getTube().interrupt();
        passengerManager.interrupt();
        appContext.close();
        Platform.exit();
    }
}
