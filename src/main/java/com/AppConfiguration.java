package com;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

@Configuration
public class AppConfiguration {
    @Bean(name="syncTube")
    public CyclicBarrier syncTubeVisualisation() {
        return new CyclicBarrier(2);
    }

    @Bean(name="syncPassenger")
    public Semaphore syncPassengerVisualisation() {
        return new Semaphore(1);
    }

}
